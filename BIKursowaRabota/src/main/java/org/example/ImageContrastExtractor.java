package org.example;

import org.apache.commons.imaging.*;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.stream.Stream;

public class ImageContrastExtractor {

    public static HashMap<String, Double> getImageCollection(){

        Path dirPath = Paths.get("images");
        HashMap<String, Double> collection = new HashMap<>();

        try (Stream<Path> paths = Files.walk(dirPath)) {
            paths.filter(Files::isRegularFile).forEach(file -> {
                // Process each file
                try {
                    BufferedImage image = Imaging.getBufferedImage(file.toFile());

                    double contrast = calculateContrast(image);
                    System.out.println("Content of " + file.getFileName() + ":\n" + "Contrast is: " + contrast);
                    collection.put(file.toFile().getAbsolutePath(), contrast);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ImageReadException e) {
                    throw new RuntimeException(e);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return collection;
    }

    private static double calculateContrast(BufferedImage image) {
        // Variables to hold the sum and number of pixels
        double sum = 0, totalPixels = image.getWidth() * image.getHeight();
        int min = 255, max = 0;

        // Traverse through each pixel
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                int color = image.getRGB(x, y);
                int gray = (color >> 16) & 0xff;  // Extract the red channel as grayscale intensity

                // Update min and max values
                if (gray < min) min = gray;
                if (gray > max) max = gray;

                // Sum the intensities
                sum += gray;
            }
        }

        // Calculate the mean
        double mean = sum / totalPixels;

        // Calculate variance
        double variance = 0;
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                int color = image.getRGB(x, y);
                int gray = (color >> 16) & 0xff;
                variance += Math.pow(gray - mean, 2);
            }
        }

        // Finalize the standard deviation calculation
        double stdDev = Math.sqrt(variance / totalPixels);

        // Return the contrast measure (using standard deviation here)
        return stdDev;
    }
}
