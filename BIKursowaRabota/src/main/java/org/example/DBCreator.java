package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class DBCreator {

    public static void main(String[] args) {
        HashMap<String, Double> collection = ImageContrastExtractor.getImageCollection();
        String dbUrl = "jdbc:mariadb://localhost:3306/bioinformatic_covid";
        String dbName = "root";
        String dbPass = "root";

        String insertSQL = "INSERT INTO bioinformatic_covid.images (absolutePath, contrast) VALUES (?, ?)";

        try(Connection connection = DriverManager.getConnection(dbUrl, dbName, dbPass);
            PreparedStatement preparedStatement = connection.prepareStatement(insertSQL)){
            for (Map.Entry<String, Double> entry : collection.entrySet()) {
                preparedStatement.setString(1, entry.getKey());
                preparedStatement.setDouble(2, entry.getValue());
                preparedStatement.executeUpdate();
            }
        }catch (SQLException e){
            e.getStackTrace();
        }
    }
}
