package org.example;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Scanner;
import java.awt.Desktop;


public class AdaptiveContrastSearch {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the contrast parameter you want to search by: \n");
        String input = scanner.nextLine();
        HashMap<String, Double> result;
        double contrastParam = 0.0;
        try{
            contrastParam = Double.parseDouble(input);
        }catch (NumberFormatException e){
            System.out.println("Invalid parameter.");
        }
        result = searchFiles(contrastParam);

        for (String path : result.keySet()) {
            File file = new File(path);

            if(!Desktop.isDesktopSupported()){
                System.out.println("Desktop is not supported");
                return;
            }
            Desktop desktop = Desktop.getDesktop();
            if(file.exists()){
                try {
                    desktop.open(file);
                }catch (IOException e){
                    e.printStackTrace();
                }
            }else{
                System.out.println("File does not exist!");
            }
        }


    }
    public static HashMap<String, Double> searchFiles(double contrastParam){
        HashMap<String, Double> collection = new HashMap<>();
        String dbUrl = "jdbc:mariadb://localhost:3306/bioinformatic_covid";
        String dbName = "root";
        String dbPass = "root";

        String selectSQL = "SELECT * FROM bioinformatic_covid.images ORDER BY ABS(contrast - ?) limit 5";

        try(Connection connection = DriverManager.getConnection(dbUrl, dbName, dbPass);
            PreparedStatement preparedStatement = connection.prepareStatement(selectSQL)){
            preparedStatement.setDouble(1,contrastParam);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                collection.put(resultSet.getString("absolutePath"),
                        resultSet.getDouble("contrast"));
            }
        }catch (SQLException e){
            e.getStackTrace();
        }
        return collection;
    }
}
